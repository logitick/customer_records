<?php

namespace CustomerRecords\Service;

use CustomerRecords\Entities\Location;

class DistanceService
{
    public function getKmDistance(Location $a, Location $b)
    {
        $dLatitude = $b->getLatitude() - $a->getLatitude();
        $dLongitude = $b->getLongitude() - $b->getLongitude();
    }


    public function degtoRad(float $deg) : float
    {
        return $deg * pi() / 180;
    }
}
