<?php


namespace CustomerRecords\Data;


class DataSource
{
    private $dataUrl;
    private $raw;

    public function __construct(string $dataUrl)
    {
        $this->dataUrl = $dataUrl;
        $this->raw = $this->readSourceContents();
    }

    public function getData() : array
    {
        return explode("\n", $this->raw);
    }

    public function parseJson(string $json) : array
    {
        return json_decode($json, JSON_OBJECT_AS_ARRAY);
    }


    public function readSourceContents() : string
    {
        return file_get_contents($this->dataUrl);
    }
}
