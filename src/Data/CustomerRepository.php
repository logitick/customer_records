<?php

namespace CustomerRecords\Data;


use CustomerRecords\Entities\Customer;
use CustomerRecords\Entities\Location;

class CustomerRepository implements Repository
{
    private $dataSource;

    public function __construct(DataSource $dataSource)
    {
        $this->dataSource = $dataSource;
    }

    /**
     * @return Customer[]
     */
    function findAll() : array
    {
        $customers = [];
        foreach ($this->dataSource->getData() as $datum) {
            $customerData = $this->dataSource->parseJson($datum);
            if ($customerData != null) {
                $location = new Location($customerData["latitude"], $customerData["longitude"]);
                $customers[] = new Customer($customerData["user_id"], $customerData["name"], $location);
            }
        }
        return $customers;
    }

}
