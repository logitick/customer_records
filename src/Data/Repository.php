<?php

namespace CustomerRecords\Data;


interface Repository
{
    function findAll() : array;
}
