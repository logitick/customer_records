<?php
namespace CustomerRecords\Entities;


class Location
{
    private $longitude;
    private $latitude;

    public function __construct(float $latitude, float $longitude)
    {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public static function fromString(string $location) : Location
    {
        list($lat, $lng) = explode(",", $location);
        return new Location((float)$lat, (float)$lng);
    }
}
