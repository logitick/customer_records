<?php

namespace CustomerRecords\Entities;


class Customer
{
    private $name;
    private $location;
    private $id;

    public function __construct(int $id, string $name, Location $location)
    {
        $this->id = $id;
        $this->name = $name;
        $this->location = $location;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getLocation(): Location
    {
        return $this->location;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
