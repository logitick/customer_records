<?php

namespace spec\CustomerRecords\Data;

use CustomerRecords\Data\CustomerRepository;
use CustomerRecords\Data\DataSource;
use CustomerRecords\Entities\Customer;
use CustomerRecords\Entities\Location;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class CustomerRepositorySpec extends ObjectBehavior
{
    function let(DataSource $dataSource)
    {
        $this->beConstructedWith($dataSource);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(CustomerRepository::class);
    }

    function it_returns_array_of_customers_from_datasource(DataSource $dataSource)
    {
        $dataSource->getData()->willReturn([0,1,2]);

        $id = random_int(1,1000);
        $name = uniqid();
        $lat = rand() * 0.01;
        $lng = rand() * 2.1;
        $dataSource->parseJson(Argument::any())->willReturn(
            ["latitude" => $lat, "user_id" => $id, "name" =>  $name, "longitude" => $lng]
        );

        $this->findAll()->shouldBeArray();
        $this->findAll()->shouldHaveCount(3);
        $results = $this->findAll();
        $results[0]->shouldBeLike(new Customer($id, $name, new Location($lat, $lng)));
    }

}
