<?php

namespace spec\CustomerRecords\Data;

use CustomerRecords\Data\DataSource;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class DataSourceSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(dirname(__FILE__) . "/test_file.txt");
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(DataSource::class);
    }

    function it_can_set_newlines_as_its_own_item()
    {
        $this->getData()->shouldBeArray();
        $this->getData()->shouldHaveCount(6);
        $this->getData()->shouldEqual([
            "hello",
            "world",
            "this",
            "is",
            "my",
            "file",
        ]);
    }

    function it_can_parse_json_into_array()
    {

        $id = random_int(1, 10000);
        $name = uniqid();
        $value = $this->parseJson('{"key1": 12321, "id": '.$id.', "name": "'.$name.'"}');
        $value->shouldBeArray();
        $value->shouldHaveKey("key1");
        $value->shouldHaveKey("id");
        $value->shouldHaveKey("name");
        $value->shouldEqual([
            "key1" => 12321,
            "id" => $id,
            "name" => $name
        ]);
    }
}
