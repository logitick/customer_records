<?php

namespace spec\CustomerRecords\Entities;

use CustomerRecords\Entities\Location;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LocationSpec extends ObjectBehavior
{
    function let() {
        $this->beConstructedWith(53.339428, -6.257664);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Location::class);
    }

    function it_can_parse_a_string() {
        $lat = (float)rand();
        $long = (float)rand();
        $factory = $this::fromString("$lat,$long");

        $factory->getLongitude()->shouldEqual($long);
        $factory->getLatitude()->shouldEqual($lat);
    }

    function it_can_parse_a_string_with_spaces() {
        $lat = (float)rand();
        $long = (float)rand();
        $factory = $this::fromString("$lat,  $long");

        $factory->getLongitude()->shouldEqual($long);
        $factory->getLatitude()->shouldEqual($lat);
    }

    function it_can_parse_a_string_with_negative() {
        $lat = (float)rand() * -1;
        $long = (float)rand() * -1;
        $factory = $this::fromString("$lat, $long");

        $factory->getLongitude()->shouldEqual($long);
        $factory->getLatitude()->shouldEqual($lat);
    }
}
