<?php

namespace spec\CustomerRecords\Service;

use CustomerRecords\Entities\Location;
use CustomerRecords\Service\DistanceService;
use PhpSpec\ObjectBehavior;

class DistanceServiceSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(DistanceService::class);
    }

    function it_can_get_distance_between_locations()
    {
        $this->getKmDistance(
            new Location(52.986375, -6.043701),
            new Location(52.986375, -6.043701)
        );
    }

    function it_there_is_no_distance_between_locations()
    {
        $this->getKmDistance(
            new Location(52.986375, -6.043701),
            new Location(52.986375, -6.043701)
        )->shouldReturn(0);
    }

    function it_converts_degrees_to_radians()
    {
        $this->degtoRad(0)->shouldProve(0);
        $this->degtoRad(90)->shouldProve(90);
        $this->degtoRad(180)->shouldProve(180);
        $this->degtoRad(360)->shouldProve(360);
        $deg = rand(0,360);
        $this->degtoRad($deg)->shouldProve($deg);
    }

    public function getMatchers() : array
    {
        return [
            'prove' => function($rad, $deg) {
                return $deg == round($rad * 180 / pi());
            },
        ];
    }
}
